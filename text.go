package episodes

import (
	"errors"
	"strings"
)

// Take returns from input the text between immediately after the first instance of from, ending before the first instance of to after from.
func Take(input, from, to string) (string, error) {
	pos := strings.Index(input, from)
	if pos < 0 {
		return "", errors.New("parameter `from` not found in `input`")
	}
	input = input[pos+len(from):]
	pos = strings.Index(input, to)
	if pos < 0 {
		return "", errors.New("parameter `to` not found in `input`")
	}
	return input[:pos], nil
}
