package main

import (
	"flag"
	"log"

	"gitlab.com/bazzz/episodes"
)

var (
	id     string
	season int
)

func init() {
	flag.StringVar(&id, "id", "", "The IMDB id of the show, (starts with tt).")
	flag.IntVar(&season, "s", 0, "the season number.")
	flag.Parse()
}

func main() {
	outPath := "output"
	if err := episodes.Process(id, season, outPath); err != nil {
		log.Fatal(err)
	}
}
