package episodes

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"gitlab.com/bazzz/dates"
	"gitlab.com/bazzz/downloader"
)

const (
	baseURL     = "https://www.imdb.com/title/"
	episodesURL = "/episodes?season="
)

func Process(id string, season int, outPath string) error {
	if id == "" {
		return fmt.Errorf("parameter `id` cannot be empty")
	}
	if season < 1 {
		return fmt.Errorf("parameter `s` cannot be lower than 1")
	}
	if err := os.MkdirAll(outPath, os.ModePerm); err != nil {
		return fmt.Errorf("cannot make output path %v: %w", outPath, err)
	}
	data, err := getData(baseURL + id + episodesURL + strconv.Itoa(season))
	if err != nil {
		return fmt.Errorf("could not get data: %w", err)
	}
	text := string(data)
	show, err := Take(text, "itemprop='url'>", "</a>")
	if err != nil {
		return fmt.Errorf("cannot find show title")
	}
	text, err = Take(text, "class=\"list detail eplist\">", "<hr>")
	if err != nil {
		return fmt.Errorf("could not find episode list: %w", err)
	}
	items := strings.Split(text, "<div class=\"list_item")
	for _, item := range items {
		if len(item) < 100 { // sanity check
			continue
		}
		title := getTitle(item)
		if title == "" {
			continue
		}
		episodeNumber := getEpisodeNumber(item)
		if episodeNumber < 1 {
			continue
		}
		premiered := getPremiered(item)
		plot := getPlot(item)

		episode := Episode{
			Title:     title,
			Season:    season,
			Number:    episodeNumber,
			Premiered: &premiered,
			Plot:      plot,
		}
		filename := filepath.Join(outPath, episode.Filename(show))
		data, err := episode.NFO()
		if err != nil {
			continue
		}
		if err := os.WriteFile(filename+".nfo", data, os.ModePerm); err != nil {
			continue
		}
		imageURL, err := getImageURL(item)
		if err != nil {
			continue
		}
		_ = downloader.GetLazyHTTP(imageURL, filename+"-thumb") // ignore error
	}
	return nil
}

func getData(url string) ([]byte, error) {
	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	return io.ReadAll(response.Body)
}

func getTitle(item string) string {
	text, err := Take(item, "itemprop=\"name\">", "</")
	if err != nil {
		return ""
	}
	return text
}

func getEpisodeNumber(item string) int {
	text, err := Take(item, "<meta itemprop=\"episodeNumber\" content=\"", "\"/>")
	if err != nil {
		return 0
	}
	episodeNumber, err := strconv.Atoi(text)
	if err != nil {
		return 0
	}
	return episodeNumber
}

func getPremiered(item string) dates.Date {
	text, err := Take(item, "<div class=\"airdate\">", "</div>")
	if err != nil {
		return dates.Date{}
	}
	text = strings.Trim(text, " \t\n")
	text = strings.ReplaceAll(text, ".", "")
	t, err := time.Parse("2 Jan 2006", text)
	if err != nil {
		return dates.Date{}
	}
	d, err := dates.New(t.Year(), int(t.Month()), t.Day())
	if err != nil {
		return dates.Date{}
	}
	return d
}

func getPlot(item string) string {
	text, err := Take(item, " itemprop=\"description\">", "</div>")
	if err != nil {
		return ""
	}
	return strings.Trim(text, " \t\n")
}

func getImageURL(item string) (string, error) {
	imageHTML, err := Take(item, "<div class=\"image\">", "</a>")
	if err != nil {
		return "", fmt.Errorf("could not get imageURL: %w", err)
	}
	imageURL, err := Take(imageHTML, "src=\"https:", "\">")
	if err != nil {
		return "", fmt.Errorf("could not get imageURL: %w", err)
	}
	imageURL = "https:" + imageURL
	return imageURL, nil
}
