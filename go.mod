module gitlab.com/bazzz/episodes

go 1.18

require (
	gitlab.com/bazzz/dates v0.0.0-20220222123917-6929d3f6e4fa
	gitlab.com/bazzz/downloader v0.0.0-20211018135835-bb5e3e40b7be
	gitlab.com/bazzz/file v0.0.0-20220303080129-7b06d0a2508e
)
